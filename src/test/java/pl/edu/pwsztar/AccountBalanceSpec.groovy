package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification {

    @Unroll
    def "should return cash for #account account"(){
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1,100)
            bank.deposit(2, 50)
        when: "get account balance"
            def balance = bank.accountBalance(account)
        then: "check account balanace"
            balance == cash
        where:
        account | cash
           1    | 100
           2    |  50
           3    | 200
    }
}
