package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    @Unroll
    def "should withdraw #cashToWithdraw of #cash from account #accountNumber"(){

        given: "initial data"
            def bank = new Bank();
            bank.createAccount();
            bank.deposit(1,300)
            bank.createAccount();
            bank.deposit(2,50)
        when: "withdraw cash"
        def result = bank.withdraw(accountNumber, cashToWithdraw)
        then: "check withdraw"
            result

        where:
        accountNumber | cash | cashToWithdraw
              1       |  200 |      100
              2       |  0   |      0
              3       |  100 |      100
    }
}
