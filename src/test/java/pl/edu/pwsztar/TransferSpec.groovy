package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    @Unroll
    def "should transfer from #accountSource to #accountDestin"() {

        given: "Initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1,300)
            bank.deposit(2,100)
        when: "transfer money"
            def result = bank.transfer(accountSource, accountDestin, cash)
        then: "check transfer"
            result
        where:
        accountSource | accountDestin | cash
              1       |       2       | 200
              3       |       1       | 400
              4       |       1       |  10
    }
}
