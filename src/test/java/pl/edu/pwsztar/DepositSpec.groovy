package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {
    @Unroll
    def "should return #despoit deposit"(){

        given: "inital data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
        when: "deposit cash to account"
            def result = bank.deposit(accountNumber, deposit)
        then: "check deposit"
            result
            where:
            accountNumber | deposit
                  1       |   100
                  2       |   250
                  3       |   519
    }
}
