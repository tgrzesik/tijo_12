package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountsBalance extends Specification {

    @Unroll
    def "should sum cash from all accounts"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.deposit(1,100)
            bank.createAccount()
            bank.deposit(2,250)
        when: "sum balance from all accounts"
            def result = bank.sumAccountsBalance()
        then: "check sum"
            result == 350
    }
}
