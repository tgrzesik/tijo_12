package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should delete account number #accountNumber"() {

        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
        when: "the account is deleted"
            def number = bank.deleteAccount()
        then: "check deleted account cash"
            number == expected
        where:
            accountNumber | expected
                 1        |     0
                 2        |     0
                 4        |    -1
    }
}
