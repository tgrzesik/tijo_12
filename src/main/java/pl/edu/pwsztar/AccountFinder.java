package pl.edu.pwsztar;

import java.util.List;

public class AccountFinder {
    static int find(List<Account> accounts, int accountNumber){
        int counter = 0;
        int pos = -2;
        for(Account account : accounts){
            if(account.getAccountNumber()==accountNumber){
                pos = counter;
            }
            counter++;
        }
        return pos;
    }
}
