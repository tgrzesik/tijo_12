package pl.edu.pwsztar;

import java.util.LinkedList;
import java.util.List;

class Bank implements BankOperation {

    private static int accountNumber = 0;
    private List<Account> accountList = new LinkedList<>();

    public int createAccount() {
        accountNumber++;
        accountList.add(new Account(accountNumber,0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int pos = AccountFinder.find(accountList,accountNumber);
        if(pos == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        else {
            int balance = accountList.get(pos).getBalance();
            accountList.remove(pos);
            return balance;
        }
    }


    public boolean deposit(int accountNumber, int amount) {
        int pos = AccountFinder.find(accountList,accountNumber);
        if(pos == -2){
            return false;
        } else {
            accountList.get(pos).setBalance(accountList.get(pos).getBalance()+amount);
            return true;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        int pos = AccountFinder.find(accountList,accountNumber);
        if(pos == -2){
            return false;
        }
        if(accountList.get(pos).getBalance()<amount){
            return false;
        }
        accountList.get(pos).setBalance(accountList.get(pos).getBalance()-amount);
        return true;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        int posSource = AccountFinder.find(accountList,fromAccount);
        int posDestiny = AccountFinder.find(accountList,toAccount);

        if(posDestiny == -2 || posSource == -2){
            return false;
        }
        if(accountList.get(posSource).getBalance()<amount){
            return false;
        }

        accountList.get(posSource).setBalance(accountList.get(posSource).getBalance()-amount);
        accountList.get(posDestiny).setBalance(accountList.get(posDestiny).getBalance()+amount);
        return true;
    }

    public int accountBalance(int accountNumber) {
        int pos = AccountFinder.find(accountList,accountNumber);
        if (pos == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        return accountList.get(pos).getBalance();
    }

    public int sumAccountsBalance() {
        int result  = 0;
        for(Account account: accountList){
            result += account.getBalance();
        }
        return result;
    }
}
